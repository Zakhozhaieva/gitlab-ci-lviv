package com.epam.gitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabCiApplication.class, args);
    }

}
